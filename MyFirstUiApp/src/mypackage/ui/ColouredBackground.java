package mypackage.ui;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class ColouredBackground extends HorizontalFieldManager{

	Background background;
	int fg_colour;
	
	ColouredBackground(int bg_colour, int fg_colour, long style){
		super(style);
		background = BackgroundFactory.createSolidBackground(bg_colour);
		setBackground(background);
		this.fg_colour = fg_colour;
	}
	
	// this will change the colour of the text
	protected void paint(Graphics graphics) {
		graphics.setColor(fg_colour);
		super.paint(graphics);
	}
}
