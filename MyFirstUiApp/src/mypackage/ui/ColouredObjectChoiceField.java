package mypackage.ui;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ObjectChoiceField;

public class ColouredObjectChoiceField extends ObjectChoiceField {

	int fg_colour;

	ColouredObjectChoiceField(int fg_colour,String[] choices, String label, int initialIndex, long style) {
		super(label, choices, initialIndex, style);
		this.fg_colour = fg_colour;
	}


	// this will change the colour of the text
	protected void paint(Graphics graphics) {
		graphics.setColor(fg_colour);
		super.paint(graphics);
	}

}
