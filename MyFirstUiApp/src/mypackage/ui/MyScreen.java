package mypackage.ui;

import mypackage.GreetingsDetails;
import mypackage.Constants;
import mypackage.command.MenuCommandHandler;
import net.rim.device.api.command.Command;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontManager;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.RadioButtonField;
import net.rim.device.api.ui.component.RadioButtonGroup;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.container.GridFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.StringProvider;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyScreen extends MainScreen {
	private LabelField greeting = new LabelField("Hello and Welcome ;)");

	private LabelField title_label = new LabelField("Title :",
			Field.FIELD_RIGHT);
	// private EditField title = new EditField("", "Mr");

	String[] title_choices = { "Mr", "Ms", "Miss" };
	// private ObjectChoiceField title_label_choice = new ObjectChoiceField("",
	// title_choices, 1, Field.FIELD_LEFT);
	private ColouredObjectChoiceField title_label_choice = new ColouredObjectChoiceField(
			Color.GREEN, title_choices, "", 1, Field.FIELD_LEFT);

	private LabelField firstname_label = new LabelField("First Name :",
			Field.FIELD_RIGHT);
	private EditField firstname = new EditField(TextField.NO_NEWLINE);
	private LabelField surname_label = new LabelField("Surame :",
			Field.FIELD_RIGHT);
	private EditField surname = new EditField(TextField.NO_NEWLINE);

	// gender radio buttons
	private LabelField gender_label = new LabelField("Gender:",
			Field.FIELD_RIGHT);
	private String[] genders = { "male", "female" };
	private RadioButtonGroup gender_radio_button_group = new RadioButtonGroup();
	private RadioButtonField male_radio_button = new RadioButtonField("male",
			gender_radio_button_group, true);
	private RadioButtonField female_radio_button = new RadioButtonField(
			"female", gender_radio_button_group, false);

	// private ButtonField submitButton = new ButtonField("Submit",
	// Field.FIELD_HCENTER| ButtonField.CONSUME_CLICK);
	private ColouredButton submitButton = new ColouredButton("Submit",
			Color.RED, Field.FIELD_HCENTER | ButtonField.CONSUME_CLICK);

	// field change listener
	private FieldChangeListener listener = new FieldChangeListener() {

		public void fieldChanged(Field field, int context) {
			String gender = "Unknown";
			int selected_gender = gender_radio_button_group.getSelectedIndex();
			if (selected_gender > -1) {
				gender = genders[selected_gender];
			}
			Dialog.inform("Greetings " + getChoiceName() + " " + firstname
					+ " " + surname + ". According to our records your sex is "
					+ gender + ".");

			GreetingsDetails greetingsDetails = new GreetingsDetails(
					getChoiceName(), firstname.toString(), surname.toString(),
					gender);

			saveGreetingsDetails(greetingsDetails);

			Dialog.inform("Greetins Details stored?" + greetingsDetails.getTitle() + greetingsDetails.getFirstName() +
					greetingsDetails.getSurname() + greetingsDetails.getGender());
		}
	};

	private MenuItem mi_my_menu_item = new MenuItem(new StringProvider(
			"Personal Greeting Tomek"), 110, 10);

	private Font myfont;

	// field managers
	// private HorizontalFieldManager horizontal_manager = new
	// HorizontalFieldManager(
	// Field.USE_ALL_HEIGHT | Field.USE_ALL_WIDTH);
	// private VerticalFieldManager vertical_left = new VerticalFieldManager();
	// private VerticalFieldManager vertical_right = new VerticalFieldManager();

	public MyScreen() {
		// Set the displayed title of the screen
		setTitle("MyTitle v1.11");

		setFontSize(40);

		// Retrieve previous greetings
		GreetingsDetails oldGreetingsDetails = null;
		oldGreetingsDetails = getGreetingsDetails();
		if (oldGreetingsDetails != null) {
			greeting.setText("Hello " +  oldGreetingsDetails.getTitle() + " " + oldGreetingsDetails.getFirstName()
					+ " " + oldGreetingsDetails.getSurname());
			firstname.setText(oldGreetingsDetails.getFirstName().toString()) ;
			surname.setText(oldGreetingsDetails.getSurname().toString()) ;
		} else {
			greeting.setText("Hellooo NEW ;)");
		}

		ColouredBackground colouredBackground = new ColouredBackground(
				Color.ORANGE, Color.RED, Field.USE_ALL_HEIGHT
						| Field.USE_ALL_WIDTH);

		// use grid instead of field managers
		int rows = 6;
		int cols = 2;

		GridFieldManager grid_manager = new GridFieldManager(rows, cols,
				GridFieldManager.FIXED_SIZE);
		grid_manager.setColumnProperty(0, GridFieldManager.FIXED_SIZE,
				Display.getWidth() / 3);
		grid_manager.setColumnProperty(1, GridFieldManager.FIXED_SIZE,
				Display.getWidth() / 3 * 2);

		add(greeting);
		// vertical_right.add(title);
		grid_manager.add(title_label, Field.FIELD_RIGHT);
		grid_manager.add(title_label_choice, Field.FIELD_LEFT);
		grid_manager.add(firstname_label, Field.FIELD_RIGHT);
		grid_manager.add(firstname, Field.FIELD_LEFT);
		grid_manager.add(surname_label, Field.FIELD_RIGHT);
		grid_manager.add(surname, Field.FIELD_LEFT);
		grid_manager.add(gender_label, Field.FIELD_RIGHT);
		grid_manager.add(male_radio_button, Field.FIELD_LEFT);
		grid_manager.add(new NullField());
		grid_manager.add(female_radio_button, Field.FIELD_LEFT);

		// horizontal_manager.add(vertical_left);
		// horizontal_manager.add(vertical_right);
		grid_manager.add(new NullField());
		grid_manager.add(submitButton, Field.FIELD_LEFT);
		add(colouredBackground);
		colouredBackground.add(grid_manager);
		// add(grid_manager);
		// add(horizontal_manager);

		submitButton.setChangeListener(listener);

		mi_my_menu_item.setCommandContext(greeting);
		mi_my_menu_item.setCommand(new Command(new MenuCommandHandler()));
		addMenuItem(mi_my_menu_item);
	}

	private void saveGreetingsDetails(GreetingsDetails greetingsDetails) {
		synchronized (PersistentStore.getSynchObject()) {
			PersistentObject po = PersistentStore.getPersistentObject(Constants.USER_PREFS_ID);
			po.setContents(greetingsDetails);
			po.commit();
		}
	}

	private static GreetingsDetails getGreetingsDetails() {
		synchronized (PersistentStore.getSynchObject()) {
			PersistentObject po = PersistentStore.getPersistentObject(Constants.USER_PREFS_ID);
			GreetingsDetails gd = (GreetingsDetails) po.getContents();
			return gd;
		}
		// return (GreetingsDetails) po.getContents();
	}

	private void setFontSize(int size) {
		Font default_font = Font.getDefault();
		myfont = default_font.derive(Font.PLAIN, size);
		FontManager.getInstance().setApplicationFont(myfont);

	}

	private String getChoiceName() {
		int title_id = title_label_choice.getSelectedIndex();
		return title_choices[title_id];
	}

	// override this method with return true, otherwise the application will ask
	// you some questions when you exit it
	protected boolean onSavePrompt() {
		return true;
	}

	public void close() {
		Dialog.alert("Googbye!");
		super.close();
	};
}
