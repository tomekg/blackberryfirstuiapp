package mypackage.ui;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ButtonField;

public class ColouredButton extends ButtonField{

	int fg_colour;

	public ColouredButton(String title, int fg_colour, long style) {
		super(title, style);
		this.fg_colour = fg_colour;
	}

	// this will change the colour of the text
	protected void paint(Graphics graphics) {
		graphics.setColor(fg_colour);
		super.paint(graphics);
	}

}
