package mypackage;

public class GreetingsDetails {

	private String title = "";
	private String firstName = "";
	private String surname = "";
	private String gender = "";
	
	public GreetingsDetails(String title, String firstName, String surname,
			String gender) {
		super();
		this.title = title;
		this.firstName = firstName;
		this.surname = surname;
		this.gender = gender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
