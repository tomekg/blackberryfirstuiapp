package mypackage.command;

import net.rim.device.api.command.CommandHandler;
import net.rim.device.api.command.ReadOnlyCommandMetadata;
import net.rim.device.api.ui.component.LabelField;

public class MenuCommandHandler extends CommandHandler {

	public void execute(ReadOnlyCommandMetadata metadata, Object context) {
		// TODO Auto-generated method stub
		if (context instanceof LabelField) {
			LabelField label = (LabelField) context;
			label.setText("Hello Tomek!");
		}
	}

}
